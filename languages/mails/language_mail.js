const baseHelper = require('../../app/helpers/BaseHelper');

module.exports = async function (lg) {
    try {
        let path = '';
        switch (lg) {
            case 'en':
                path = './languages/mails/en.json';
                break;
            case 'vi':
                path = './languages/mails/vi.json';
                break;
            default:
                path = './languages/mails/en.json';
        }
        const json = baseHelper.fileToJSON(path);
        return json;
    } catch (err) {
        logStack(err);
        return baseHelper.fileToJSON('./languages/mails/en.json');
    }
};
