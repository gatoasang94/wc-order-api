const baseHelper = require('../app/helpers/BaseHelper');

module.exports = function (lg) {
    try {
        let path = '';
        switch (lg) {
            case 'en':
                path = './languages/en.json';
                break;
            case 'vi':
                path = './languages/vi.json';
                break;
            default:
                path = './languages/vi.json';
        }
        const json = baseHelper.fileToJSON(path);
        return json;
    } catch (err) {
        logStack(err);
        return baseHelper.fileToJSON('./languages/vi.json');
    }
};
