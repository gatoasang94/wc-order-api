const emailService = require('./services/MailService');
const baseHelper = require('./app/helpers/BaseHelper');

test();

async function test() {
    const url = baseHelper.getWordpressUpdateProductURL('svgfabulous.store');
    const options = {
        method: 'POST',
        url,
        json: {
            sku: 'xxxx',
            attachment_url: 'https://abc.com',
            file_name: 'xxx.zip'
        }
    };
    const rs = await baseHelper.postRequest(options);
    console.log(rs);
}
