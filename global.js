if (!('toJSON' in Error.prototype)) {
    Object.defineProperty(Error.prototype, 'toJSON', {
        value: function () {
            var alt = {};

            Object.getOwnPropertyNames(this).forEach(function (key) {
                alt[key] = this[key];
            }, this);

            return alt;
        },
        configurable: true,
        writable: true
    });
}

module.exports = () => {
    global._ = require('lodash');

    global.requestID = 0;
    global.statusCode = require('./config/http_status');

    global.monitor = require('./app/helpers/Monitor');

    global.response = async function (res, data, message, code) {
        const json = JSON.stringify({
            data,
            message,
            code
        });
        res.writeHeader(200, { 'Content-Type': 'application/json; charset=utf-8' });
        res.write(json);
        res._statusCode = code;
        res.end();
        return res;
    };

    global.logRequest = function (req, res, executeTime) {
        try {
            const log = monitor.getLogger('request');
            const logsAPI = monitor.getLogger('logsAPI');
            let ipAdress = '';
            const forwardedIpsStr = req.header('x-forwarded-for');
            if (forwardedIpsStr) {
                ipAdress = forwardedIpsStr.split(',')[0];
            } else {
                ipAdress = req.connection.remoteAddress;
            }
            let params = {};
            switch (req.method) {
                case 'POST':
                    params = req.body;
                    break;
                case 'GET':
                    params = req.query;
                    break;
                default:
                    params = {};
            }
            const objLog = {
                params,
                executeTime
            };
            const strLog = `${process.env.name}-${process.env.NODE_ENV} ${ipAdress} ${req.method} ${req.path} ${res._statusCode} ${JSON.stringify(objLog)}`;
            if (!_.includes(['production'], process.env.NODE_ENV)) {
                return;
            }
            
            log.info(strLog);
            logsAPI.info(strLog);
        } catch (e) {
            console.log(e);
        }
    };
 };
