const baseHelper = require('../helpers/BaseHelper');
const rp = require('request-promise');
const cheerio = require('cheerio');

async function test() {
    const total = await getTotalPage('https://svgmassive.com/product-category/trending-svg/page/1/');
    console.log(total);
}

async function getTotalPage(url) {
    try {
        const $ = await rp({
            uri: url,
            transform: function (body) {
                return cheerio.load(body);
            }
        });
        const listPages = $('.page-numbers > li');
        const lastPage = $(listPages[listPages.length - 2]);
        return parseInt(lastPage.find('a').text());
    } catch (err) {
        console.log(err);
        return 1;
    }
}

test();