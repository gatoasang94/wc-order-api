const log4js = require('log4js');
const telegramBotApi = require('telegram-bot-api');

const cf = require('../../config/config');

const BOT_TOKEN = '779122427:AAGEw2BrUGzVLZDzNT4HU7oxo94dznN9Xr4';
const TeleBot = new telegramBotApi({ token: BOT_TOKEN });

const GROUP_ID = {
    LADI_UID: '-365775854',
    LADI_FLOW: '-253359338',
    LADI_PAGE: '-289214013',
    LADI_GLOBAL_SECTION: '-379641990',
    LADI_DNS_SSL: '399428041',
};
const APP_NAME = 'LADI_FLOW';

class Monitor {
    constructor() {

        log4js.configure({
            pm2: true,
            // pm2InstanceVar: 'INSTANCE_ID',
            appenders: {
                error: {
                    type: 'file',
                    filename: './logs/error/error.log', // specify the path where u want logs folder error.log
                    category: 'error',
                    maxLogSize: 50000000,
                    pattern: 'yyyy-MM-dd',
                    backups: 10
                },
                request: {
                    type: 'file',
                    filename: './logs/request/request.log', // specify the path where u want logs folder error.log
                    category: 'request',
                    pattern: 'yyyy-MM-dd',
                    maxLogSize: 50000000,
                    backups: 10
                },
                data: {
                    type: 'file',
                    filename: './logs/data/data.log', // specify the path where u want logs folder error.log
                    category: 'data',
                    pattern: 'yyyy-MM-dd',
                    maxLogSize: 50000000,
                    backups: 10
                },
                func: {
                    type: 'file',
                    filename: './logs/func/func.log', // specify the path where u want logs folder error.log
                    category: 'func',
                    pattern: 'yyyy-MM-dd',
                    maxLogSize: 50000000,
                    backups: 10
                },
                debug: {
                    type: 'file',
                    filename: './logs/debug/debug.log', // specify the path where u want logs folder error.log
                    category: 'debug',
                    pattern: '.yyyy-MM-dd',
                    maxLogSize: 50000000,
                    backups: 10
                },
                logsAPI: {
                    type: '@log4js-node/logstashudp',
                    host: cf.SERVER.LOG_CENTER,
                    category: APP_NAME,
                    port: 5049
                },
                logError: {
                    type: '@log4js-node/logstashudp',
                    host: cf.SERVER.LOG_CENTER,
                    category: APP_NAME,
                    port: 5050
                },
            },
            categories: {
                default: {
                    appenders: ['debug'],
                    level: 'INFO'
                },
                request: {
                    appenders: ['request'],
                    level: 'DEBUG'
                },
                error: {
                    appenders: ['error'],
                    level: 'DEBUG'
                },
                func: {
                    appenders: ['func'],
                    level: 'DEBUG'
                },
                data: {
                    appenders: ['data'],
                    level: 'DEBUG'
                },
                logsAPI: {
                    appenders: ['logsAPI'],
                    level: 'DEBUG'
                },
                logError: {
                    appenders: ['logError'],
                    level: 'INFO'
                }

            }
        });
    }

    getLogger(type) {
        return log4js.getLogger(type);
    }

    async notifySlowRequest(req, executeTime) {
        try {

        } catch (err) {
            console.log(err);
        }
    }

    async notifyErrorFunction(strLog) {
        try {

            const objMessage = {
                chat_id: GROUP_ID[APP_NAME],
                text: strLog
            };

            const env = process.env.NODE_ENV;
            if (env == 'production') {
                TeleBot.sendMessage(objMessage).then((data) => {
                    // console.log(data);
                }).catch((erre) => {
                    // console.log(err);
                });
            }
        } catch (err) {
            console.log(err);
        }
    }
}
module.exports = new Monitor();
