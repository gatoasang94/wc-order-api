const fs = require('fs');
const md5 = require('md5');
const guidid = require('uuid/v4');
const xml2js = require('xml2js');
const crypto = require('crypto');
const request = require('request');
const date = require('date-and-time');
const dateFormat = require('dateformat');
const { exec } = require('child_process');
const joiValidator = require('joi');
const moment = require('moment');
const url = require('url');
const extfs = require('extfs');
const cf = require('../../config/config');
const appConfig = require('../../config/app');

class BaseHelper {
    getCurrentDatetime(format = appConfig.DB_DATE_TIME_FORMAT) {
        return moment().format(format);
    }

    generateImageTime() {
        return this.getCurrentDatetime(cf.AMAZON.IMAGE.FORMAT_DATE_IMAGE);
    }

    extIsImage(ext) {
        if (!ext) {
            return false;
        }
        if (!_.includes(['.png', '.jpg', '.gif', '.jpeg'], ext.toLowerCase())) {
            return false;
        }

        return true;
    }

    isvalidUID(UID) {
        try {
            const regex = new RegExp(/^[0-9a-fA-F]{24}$/);
            if (regex.test(UID)) {
                return true;
            }
            return false;
        } catch (err) {
            throw err;
        }
    }

    isObjectId(id) {
        try {
            return {
                seconds: parseInt(id.slice(0, 8), 16),
                machineIdentifier: parseInt(id.slice(8, 14), 16),
                processId: parseInt(id.slice(14, 18), 16),
                counter: parseInt(id.slice(18, 24), 16)
            };
        } catch (err) {
            throw err;
        }
    }

    capitalize(s) {
        try {
            if (typeof s !== 'string') {
                return '';
            }
            return s.charAt(0).toUpperCase() + s.slice(1);
        } catch (err) {
            throw err;
        }
    }

    filterColumns(source = {}, columns = {}, destination = {}) {
        const columnNames = _.keys(columns);
        _.map(columnNames, (columnName) => {
            if (source[columnName] != undefined) {
                destination[columnName] = source[columnName];
            }
        });

        return destination;
    }

    firstDay(d) {
        try {
            d.setHours(0, 0, 1);
            return d;
        } catch (err) {
            throw err;
        }
    }

    lastDay(d) {
        try {
            d.setHours(23, 59, 59);
            return d;
        } catch (err) {
            throw err;
        }
    }

    firstMonth(d) {
        try {
            d.setHours(0, 0, 1);
            d.setDate(1);
            return d;
        } catch (err) {
            throw err;
        }
    }

    addSecond(theDate, second) {
        try {
            return date.addSeconds(theDate, second);
        } catch (err) {
            throw err;
        }
    }

    minusSecond(theDate, second) {
        try {
            return date.addSeconds(theDate, -second);
        } catch (err) {
            throw err;
        }
    }

    addMinute(theDate, minute) {
        try {
            return date.addMinutes(theDate, minute);
        } catch (err) {
            throw err;
        }
    }

    minusMinute(theDate, minute) {
        try {
            return date.addMinutes(theDate, -minute);
        } catch (err) {
            throw err;
        }
    }

    addHour(theDate, hours) {
        try {
            return date.addHours(theDate, hours);
        } catch (err) {
            throw err;
        }
    }

    minusHour(theDate, hours) {
        try {
            return date.addHours(theDate, -hours);
        } catch (err) {
            throw err;
        }
    }

    addDay(theDate, days) {
        // days phải là số nếu không trước khi truyền vào hãy dùng hàm parseInt(days)
        return date.addDays(theDate, days);
    }

    minusDay(theDate, days) {
        try {
            // days phải là số nếu không trước khi truyền vào hãy dùng hàm parseInt(days)
            return date.addDays(theDate, -days);
        } catch (err) {
            throw err;
        }
    }

    addMonth(theDate, months) {
        // months phải là số nếu không trước khi truyền vào hãy dùng hàm parseInt(months)
        return date.addMonths(theDate, months);
    }

    getDateByTimeZone(timeZone) {
        try {
            let tz = appConfig.DEFAULT_TIME_ZONE;
            if (timeZone !== undefined && timeZone != null)
                tz = timeZone;

            const date = this.addHour(new Date(), tz);
            return date;
        } catch (err) {
            logStack(err);
            return new Date();
        }
    }

    diffDate(dt1, dt2) { // So sánh ngày dt1 hơn ngày dt2 bao nhiêu giây
        return (dt1.getTime() - dt2.getTime()) / 1000;
    }

    diffDay(dt1, dt2) {
        return ((dt1.getTime() - dt2.getTime()) / 1000) / 86400;
    }

    diffDateMinute(dt1, dt2) { // So sánh ngày dt1 hơn ngày dt2 bao nhiêu phut
        return ((dt1.getTime() - dt2.getTime()) / 1000) / 60;
    }

    formatDate(date, format) { // dd-mm-yyy HH-MM-ss l
        return dateFormat(date, format);
    }

    formatDateUTC(date, format) { // dd-mm-yyy HH-MM-ss l
        return dateFormat(date, format, true);
    }

    stringToDateTime(text, format) { // DD/MM/YYYY HH:mm:ss
        try {
            return date.parse(text, format);
        } catch (err) {
            return null;
        }
    }

    decodeToken(token) {
        try {
            const decoded = this.textDecrypt(token, cf.TOKEN.SECRET_KEY);
            const objUser = JSON.parse(decoded);
            return objUser;
        } catch (err) {
            return false;
        }
    }

    parseUrl(urlFull) {
        try {
            const parsed = {
                http: '',
                domain: '',
                subFolder: ''
            };
            if (urlFull === null || !urlFull || urlFull.length === 0) {
                return parsed;
            }
            urlFull = urlFull.replace(' ', '').toLowerCase();
            let index = urlFull.indexOf('://');
            let endURL = urlFull;
            if (index > -1) {
                parsed.http = urlFull.substr(0, index).trim();
                endURL = urlFull.substr(index + 3, urlFull.length).trim();
            }
            index = endURL.indexOf('/');
            if (index == -1) {
                parsed.domain = endURL.trim().replace(' ', '');
            } else {
                parsed.domain = endURL.substr(0, index).trim();
                endURL = endURL.substr(index + 1, endURL.length).trim();
                index = endURL.indexOf('/');
                parsed.subFolder = index == -1 ? endURL : endURL.substr(0, index).trim();
            }
            parsed.domain = this.sanitizeUTF8(parsed.domain);
            return parsed;
        } catch (err) {
            throw err;
        }
    }

    parseFileName(urlString) {
        try {
            const url = require('url');
            const path = require('path');
            const parsed = url.parse(urlString);
            return path.basename(parsed.pathname);
        } catch (err) {
            throw err;
        }
    }

    async base64ToPNG(base64, name) {
        return new Promise(((resolve, reject) => {
            try {
                const base64Data = base64.replace(/^data:image\/png;base64,/, '');
                const filename = './temp/' + name + '.png';
                fs.writeFile(filename, base64Data, 'base64', (err) => {
                    if (err)
                        resolve(false);
                    resolve(filename);
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    uniqueArrayValue(value, index, self) {
        return self.indexOf(value) === index;
    }

    async getRequest(url) {
        const options = {
            method: 'GET',
            url: url
        };
        return new Promise(((resolve, reject) => {
            try {
                request(options, (error, res, body) => {
                    try {
                        if (!error && res && res.statusCode == 200) {
                            let rs = body;
                            if (typeof rs === 'string')
                                rs = JSON.parse(rs.trim());
                            resolve(rs);
                        } else {
                            reject(error);
                        }
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    async getRequestOptions(options) {
        return new Promise((resolve, reject) => {
            try {
                request(options, (error, res, body) => {
                    try {
                        if (res && (res.statusCode == 200 || res.statusCode == 201)) {
                            let rs = body;
                            if (typeof rs === 'string')
                                rs = JSON.parse(rs.trim());
                            resolve(rs);
                        } else {
                            if (!error) {
                                let _err = JSON.stringify(body);
                                if (options.url)
                                    _err += options.url;
                                error = _err;
                            }
                            // eslint-disable-next-line prefer-promise-reject-errors
                            reject({ error: error });
                        }
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async getRequestPureOptions(options) {
        return new Promise((resolve, reject) => {
            try {
                request(options, (error, res) => {
                    try {
                        resolve(res);
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async getRequestHtml(url) {
        const options = {
            method: 'GET',
            url: url
        };
        return new Promise((resolve, reject) => {
            try {
                request(options, (error, res, body) => {
                    try {
                        if (error)
                            resolve(error);
                        resolve(body);
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async getRequestPure(url) {
        const options = {
            method: 'GET',
            url: url
        };
        return new Promise((resolve, reject) => {
            try {
                request(options, (error, res) => {
                    try {
                        resolve(res);
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    async postRequest(options) {
        return new Promise((resolve, reject) => {
            try {
                request(options, (error, res, body) => {
                    try {
                        if (res && (res.statusCode == 200 || res.statusCode == 201)) {
                            let rs = body;
                            if (typeof rs === 'string')
                                rs = JSON.parse(rs.trim());
                            resolve(rs);
                        } else {
                            if (!error) {
                                let _err = JSON.stringify(body);
                                if (options.url)
                                    _err += options.url;
                                error = _err;
                            }
                            // eslint-disable-next-line prefer-promise-reject-errors
                            reject({ error: error });
                        }
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                // eslint-disable-next-line prefer-promise-reject-errors
                reject({ error: err });
            }
        });
    }

    sanitizeUTF8(text) {
        const vnTexts = ['á', 'à', 'ả', 'ã', 'ạ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'đ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ', 'í', 'ì', 'ỉ', 'ĩ', 'ị', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự', 'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Đ', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'];
        const replaceText = ['a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'y', 'y', 'y', 'y', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'D', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'Y', 'Y', 'Y', 'Y', 'Y'];
        let index;
        for (let i = 0; i < vnTexts.length; i++) {
            index = text.indexOf(vnTexts[i]);
            if (index > -1) {
                text = text.replace(new RegExp(vnTexts[i], 'g'), replaceText[i]);
            }
        }
        return text;
    }

    seoURL(name) {
        if (!name)
            return '';
        name = this.sanitizeUTF8(name);
        name = name.replace(/ /g, '-');
        name = name.replace(/[^A-Za-z0-9-_\.]/g, '');
        name = name.replace(/\.+/g, '-');
        name = name.replace(/-+/g, '-');
        name = name.replace(/_+/g, '_');
        name = name.toLowerCase();
        return name.trim();
    }

    generatePermissionCode(id) {
        const sTime = date.format(new Date(), 'YYYYMMDDHHmmss');
        return id.substring(15) + sTime;
    }

    generateTime() {
        return dateFormat(new Date(), 'yyyymmddHHMMss');
    }

    generateText(length) {
        const text = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '_', '-', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'L', 'K', 'J', 'H', 'G', 'F', 'D', 'S', 'A', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'];
        let result = '';
        for (let i = 0; i < length; i++) {
            result += text[Math.floor(Math.random() * text.length)];
        }
        return result;
    }

    generateTextToken(text) {
        try {
            return crypto.createHash('sha256').update(`${new Date().toString() + '&63Minh(84lsdf' + text + 'Cu@$Ot206'}&SALT&OTHER_PARAMS`).digest('hex');
        } catch (err) {
            throw err;
        }
    }

    getClientIP(req) {
        try {
            return req.headers['x-forwarded-for'];
        } catch (err) {
            throw err;
        }
    }

    async checkValueExistInList(value, list, name) {
        try {
            for (let i = 0; i < list.length; i++) {
                if (list[i][name] == value)
                    return true;
            }
            return false;
        } catch (err) {
            throw err;
        }
    }

    async createFolder(_path) {
        try {
            if (!fs.existsSync(_path)) {
                fs.mkdirSync(_path);
            }
        } catch (err) {
            throw err;
        }
    }

    async createFolderFull(path) {
        try {
            const list = path.split('/');
            let newPath = '/';
            for (let i = 0; i < list.length; i++) {
                newPath += list[i] + '/';
                if (!fs.existsSync(newPath)) {
                    fs.mkdirSync(newPath);
                }
            }
        } catch (err) {
            throw err;
        }
    }

    async zipFolder(pathRaw, pathNew) {
        return new Promise(((resolve, reject) => {
            try {
                const zipF = require('zip-folder');
                zipF(pathRaw, pathNew, (err) => {
                    if (err) {
                        logStack(err);
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    async trimByChar(text, c) {
        try {
            let rs = text.trim();
            while (rs.startsWith(c)) {
                rs = rs.substr(1);
            }
            while (rs.endsWith(c)) {
                rs = rs.substr(0, rs.length - 1);
            }
            return rs.trim();
        } catch (err) {
            throw err;
        }
    }

    async textToXML(text) {
        return new Promise(((resolve, reject) => {
            try {
                xml2js.parseString(text, { trim: true }, (err, result) => {
                    if (err)
                        reject(err);
                    else
                        resolve(result);
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    async writeBase64ToImage(base64, path) {
        try {
            const base64Data = base64.replace(/^data:image\/png;base64,/, '');
            fs.writeFileSync(path, base64Data, 'base64');
        } catch (err) {
            throw err;
        }
    }

    async writeFile(path, text) {
        try {
            fs.writeFileSync(path, text, 'utf8', (err) => {
                if (err)
                    throw err;
            });
        } catch (err) {
            throw err;
        }
    }

    async writeFileAppend(path, text) {
        try {
            fs.appendFile(path, text, 'utf8', (err) => {
                if (err)
                    throw err;
            });
        } catch (err) {
            throw err;
        }
    }

    async readFile(path) {
        try {
            return fs.readFileSync(path, 'utf8');
        } catch (err) {
            throw err;
        }
    }

    fileToJSON(path) {
        try {
            const content = fs.readFileSync(path, 'utf8');
            return JSON.parse(content.trim());
        } catch (err) {
            throw err;
        }
    }

    async readFileToList(path) {
        try {
            if (!fs.existsSync(path))
                return [];
            const content = fs.readFileSync(path, 'utf8');
            return content.trim().split('\n');
        } catch (err) {
            throw err;
        }
    }

    async readFileToListNotEmpty(path) {
        try {
            if (!fs.existsSync(path))
                return [];
            const content = fs.readFileSync(path, 'utf8');
            const lines = content.trim().split('\n');
            const list = [];
            let index = 0;
            for (let i = 0; i < lines.length; i++) {
                if (lines[i].trim() == '')
                    continue;
                list[index] = lines[i].trim();
                index++;
            }
            return list;
        } catch (err) {
            throw err;
        }
    }

    encryptMD5(before, text, after) {
        try {
            return md5(before + text + after);
        } catch (err) {
            throw err;
        }
    }

    async hmacText(text, secretKey) {
        try {
            return crypto.createHmac('sha256', secretKey).update(text).digest('hex');
        } catch (err) {
            throw err;
        }
    }

    textEncrypt(data, secretKey, iv = false) {
        try {
            const cipher = iv ? crypto.createCipheriv('aes-256-cbc', secretKey, iv) : crypto.createCipher('aes-256-cbc', secretKey);
            let crypted = cipher.update(data, 'utf-8', 'hex');
            crypted += cipher.final('hex');
            return crypted;
        } catch (err) {
            throw err;
        }
    }

    textDecrypt(data, secretKey, iv = false) {
        try {
            const decipher = iv ? crypto.createDecipheriv('aes-256-cbc', secretKey, iv) : crypto.createDecipher('aes-256-cbc', secretKey);
            let decrypted = decipher.update(data, 'hex', 'utf-8');
            decrypted += decipher.final('utf-8');
            return decrypted;
        } catch (err) {
            throw err;
        }
    }

    objectEncrypt(secretKey, ob) {
        try {
            const encryptor = require('simple-encryptor')({
                key: secretKey,
                hmac: false,
                debug: false
            });
            return encryptor.encrypt(ob);
        } catch (err) {
            throw err;
        }
    }

    objectDecrypt(secretKey, data) {
        try {
            const encryptor = require('simple-encryptor')({
                key: secretKey,
                hmac: false,
                debug: false
            });
            return encryptor.decrypt(data);
        } catch (err) {
            throw err;
        }
    }

    async formatNumber(number, n, x, s, c) {
        try {
            const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
            const num = number.toFixed(Math.max(0, ~~n));
            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        } catch (err) {
            throw err;
        }
    }

    async lookupDomain(ips, domain) {
        const dns = require('dns');
        return new Promise(((resolve, reject) => {
            try {
                dns.lookup(domain, async (er, addresses, family) => {
                    try {
                        if (er)
                            resolve(false);
                        let flag = false;
                        for (let index = 0; index < ips.length; index++) {
                            if (addresses === ips[index]) {
                                flag = true;
                                break;
                            }
                        }
                        resolve(flag);
                    } catch (err) {
                        logStack(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    async shuffleList(list) {
        try {
            const newList = [];
            let index;
            let i = 0;
            while (list.length > 0) {
                index = Math.floor(Math.random() * Math.floor(list.length));
                newList[i] = list[index];
                i++;
                list.splice(index, 1);
            }
            return newList;
        } catch (err) {
            throw err;
        }
    }

    async randomNumber(max) {
        try {
            return Math.floor(Math.random() * Math.floor(max));
        } catch (err) {
            throw err;
        }
    }

    async randomNumberMinMax(min, max) {
        try {
            return Math.floor(Math.random() * (max - min) + min);
        } catch (err) {
            throw err;
        }
    }

    async _guidid() {
        try {
            return guidid();
        } catch (err) {
            throw err;
        }
    }

    async copyFile(path, newPath) {
        try {
            await fs.copyFileSync(path, newPath);
        } catch (err) {
            throw err;
        }
    }

    async moveFile(oldPath, pathTo) {
        try {
            await fs.renameSync(oldPath, pathTo);
        } catch (err) {
            throw err;
        }
    }

    async deleteFolder(path) {
        try {
            await fs.rmdirSync(path);
        } catch (err) {
            throw err;
        }
    }

    async deleteFile(path) {
        try {
            await fs.unlinkSync(path);
        } catch (err) {
            throw err;
        }
    }

    async deleteAllFileInFolder(path, Seconds) {
        try {
            if (!path.endsWith('/'))
                path += '/';
            const files = fs.readdirSync(path);
            const cur = new Date();
            for (let i = 0; i < files.length; i++) {
                try {
                    const info = fs.statSync(path + files[i]);
                    const createTime = new Date(info.birthtime);
                    if (this.diffDate(cur, createTime) > Seconds) {
                        fs.unlinkSync(path + files[i]);
                    }
                } catch (e) { }
            }
        } catch (err) {
            throw err;
        }
    }

    async listFileInFolder(path) {
        try {
            const { statSync, readdirSync } = require('fs');
            const { join } = require('path');
            const dirs = p => readdirSync(p).filter(f => statSync(join(p, f)).isFile());
            return dirs(path);
        } catch (err) {
            logStack(err);
        }
        return [];
    }

    async listFolderInFolder(path) {
        try {
            const { statSync, readdirSync } = require('fs');
            const { join } = require('path');
            const dirs = p => readdirSync(p).filter(f => statSync(join(p, f)).isDirectory());
            return dirs(path);
        } catch (err) {
            logStack(err);
        }
        return [];
    }

    async listAllInFolder(path) {
        try {
            return await fs.readdirSync(path);
        } catch (err) {
            throw err;
        }
    }

    async getFileMetadata(path) {
        return new Promise(((resolve, reject) => {
            try {
                fs.stat(path, (err, stats) => {
                    if (err)
                        reject(err);
                    resolve(stats);
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    async execProcess(command) {
        return new Promise(((resolve, reject) => {
            try {
                exec(command, (err, stdout, stderr) => {
                    if (err)
                        reject(err);
                    const rs = {
                        stderr: stderr,
                        stdout: stdout
                    };
                    resolve(rs);
                });
            } catch (err) {
                reject(err);
            }
        }));
    }

    trimBody(req, res, next) {
        if (req.body) {
            trimStringProperties(req.body);
        }
        next();
    }

    convert(obj) {
        // Note: cache should not be re-used by repeated calls to JSON.stringify.
        let cache = [];
        const res = JSON.stringify(obj, (key, value) => {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    // Duplicate reference found, discard key
                    return;
                }
                // Store value in our collection
                cache.push(value);
            }
            return value;
        });
        cache = null;
        return res;
    }

    validateJson(object, schema) {
        const result = joiValidator.validate(object, schema);
        return result.error === null;
    }

    getAllMonth(fromDate, toDate) {
        const dateStart = moment(fromDate);
        const dateEnd = moment(toDate);
        const timeValues = [];

        while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
            timeValues.push(dateStart.format('YYYY.MM'));
            dateStart.add(1, 'month');
        }
        return timeValues;
    }

    getCurrentDatetime(format) {
        return moment().format(format);
    }

    appendFile(path, data) {
        path.split('/').slice(0, -1).reduce((last, folder) => {
            const folderPath = last ? (last + '/' + folder) : folder;

            if (!fs.existsSync(folderPath)) {
                fs.mkdirSync(folderPath);
            }

            return folderPath;
        });

        if (!fs.existsSync(path)) {
            fs.writeFileSync(path, data, 'utf8');
        } else {
            fs.appendFile(path, data, (err) => {
                if (err) {
                    logStack(err);
                }
            });
        }
    }

    getDomain(url) {
        const match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            return match[2];
        }

        return null;
    }

    removeQueryString(urlString) {
        const obj = url.parse(urlString);
        obj.search = obj.query = '';

        return url.format(obj);
    }

    getQueryString(urlString) {
        const obj = url.parse(urlString);
        return obj.search ? obj.search.replace('?', '') : '';
    }

    validateFile(uploadType, fileName, fileSize, message) {
        const fileType = fileName.split('.')[1].toString().toLowerCase();
        let allowTypes = [];
        let notAllowTypes = [];
        let size = appConfig.UPLOAD_FILE.IMAGE.MAX_SIZE;

        switch (uploadType) {
            case appConfig.UPLOAD_FILE.IMAGE.TYPE:
                allowTypes = appConfig.UPLOAD_FILE.IMAGE.ALLOW;
                size = appConfig.UPLOAD_FILE.IMAGE.MAX_SIZE;
                break;
            case appConfig.UPLOAD_FILE.VIDEO.TYPE:
                allowTypes = appConfig.UPLOAD_FILE.VIDEO.ALLOW;
                size = appConfig.UPLOAD_FILE.IMAGE.MAX_SIZE;
                break;
            case appConfig.UPLOAD_FILE.FILE.TYPE:
                notAllowTypes = appConfig.UPLOAD_FILE.FILE.NOT_ALLOW;
                size = appConfig.UPLOAD_FILE.IMAGE.MAX_SIZE;
                break;
        }

        if (!_.isEmpty(allowTypes) && !_.includes(allowTypes, fileType.toLowerCase())) {
            return message.FILE_TYPE_NOT_ALLOW;
        }

        if (!_.isEmpty(notAllowTypes) && _.includes(notAllowTypes, fileType.toLowerCase())) {
            return message.FILE_TYPE_NOT_ALLOW;
        }

        if (size < fileSize) {
            return message.FILE_SIZE_EXCEED;
        }
    }

    generateRandomString(length) {
        const crypto = require('crypto');
        const id = crypto.randomBytes(length).toString('hex');

        return id;
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    getSignature(buf) {
        const hmac = crypto.createHmac('sha1', appConfig.FACEBOOK.APP_SECRET);
        hmac.update(buf, 'utf-8');
        return 'sha1=' + hmac.digest('hex');
    }

    getSignatureBySecretKey(text, secret) {
        const buf = Buffer.from(text, 'utf8');
        const hmac = crypto.createHmac('sha1', secret);
        hmac.update(buf, 'utf-8');
        return 'sha1=' + hmac.digest('hex');
    }

    getSortedStringFromObject(obj) {
        const ordered = {};
        Object.keys(obj).sort().forEach((key) => {
            ordered[key] = obj[key];
        });
        return JSON.stringify(ordered);
    }

    checkIsLiving(streamKey) {
        return !extfs.isEmptySync(`${cf.LIVE_STREAM_FOLDER}live/${streamKey}`);
    }

    getPostBrief(content) {
        try {
            const MAX_LENGTH = 150;

            const text = content.replace(/<[^>]*>/ig, ' ')
                .replace(/<\/[^>]*>/ig, ' ')
                .replace(/&nbsp;|&#160;/gi, ' ')
                .replace(/\s+/ig, ' ')
                .trim();

            if (text.length < MAX_LENGTH)
                return text;

            const temp = text.substring(0, MAX_LENGTH);
            const idx = temp.lastIndexOf(' ');
            return temp.substring(0, idx);
        } catch (err) {
            return content;
        }
    }

    generateMatchCode(home, away, time) {
        try {
            return this.seoURL(home) + '-vs-' + this.seoURL(away) + '-' + this.formatDate(new Date(time), 'yyyyMMdd');
        } catch (err) {
            console.log(err);
        }
    }

    isNumber(str) {
        return /^\d+$/.test(str);
    }

    removeNumber(name) {
        let temp = name;

        if (this.isNumber(name.substring(temp.length - 2, temp.length)))
            return name;

        if (this.isNumber(name.substring(0, 1)))
            temp = name.substring(1, name.length).trim();
        if (this.isNumber(temp.substring(temp.length - 1, temp.length)))
            temp = temp.substring(0, temp.length - 1).trim();

        return temp;
    }

    cutSentence(sentence, maxLength) {
        const trimmedString = sentence.substr(0, maxLength);
        return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')));
    }

    getIpV4(ip) {
        return ip.replace(/^.*:/, '');
    }

    getWordpressUpdateProductURL(domain) {
        try {
            if (domain == 'localhost')
                return 'http://localhost/wordpress/wp-json/order-checker/v1/product';
            return 'https://' + domain + '/wp-json/order-checker/v1/product';
        } catch (err) {
            console.log(err);
        }
    }
}

function trimStringProperties(obj) {
    if (obj !== null && typeof obj === 'object') {
        for (const prop in obj) {
            if (typeof obj[prop] === 'object') {
                trimStringProperties(obj[prop]);
            }

            if (typeof obj[prop] === 'string' || obj[prop] instanceof String) {
                obj[prop] = obj[prop].trim();
            }
        }
    }
}

module.exports = new BaseHelper();
