class BaseController {
    setLanguage(language) {
        this.message = require('../../languages/language')(language);
    }
}

module.exports = BaseController;
