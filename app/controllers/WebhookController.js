const baseHelper = require('../helpers/BaseHelper');
const BaseController = require('./BaseController');

// models
const productModel = require('../models/Product');
const configModel = require('../models/Config');
const orderModel = require('../models/Order');

// services
const emailService = require('../../services/MailService');

class WebhookController extends BaseController {
    async monitor(req, res, params) {
        res.status(200).send('EVENT_RECEIVED');
        try {
            const domain = await this.getDomainFromCardName(params.action.data.card.name);

            // find config
            let conditions = {
                trello_board: params.model.idBoard,
                trello_dest_list: params.model.id,
                domain
            };
            const config = await configModel.findOneRecord(conditions);

            if (!config)
                return;

            // check action is moving the card from DOING list to DONE list
            const oldList = params.action.data.old.idList;
            if (oldList == config.trello_dest_list)
                return;

            // check status send done
            const cardID = params.action.data.card.id;
            conditions = { card_id: cardID };
            const product = await productModel.findOneRecord(conditions);
            if (!product || product.is_done)
                return;

            // check if the card lastest attachment is a zip file
            let url = 'https://api.trello.com/1/cards/' + cardID + '/attachments?';
            url += 'key=' + config.trello_key;
            url += '&token=' + config.trello_token;
            const result = await baseHelper.getRequest(url);
            if (_.isEmpty(result))
                return;

            const lastestAttachment = result[result.length - 1];
            let { fileName } = lastestAttachment;
            if (!fileName) {
                fileName = baseHelper.parseFileName(lastestAttachment.url);
            }

            if (!fileName.endsWith('.zip')) {
                console.log('fileName invalid: ' + fileName);
                fileName = product.sku + '.zip';
            }
            // send attachment data back to woo to update product
            let attachmentURL;
            try {
                attachmentURL = lastestAttachment.url;
                url = baseHelper.getWordpressUpdateProductURL(product.domain);
                const options = {
                    method: 'POST',
                    url,
                    json: {
                        sku: product.sku,
                        attachment_url: attachmentURL,
                        file_name: fileName
                    }
                };
                const postRs = await baseHelper.postRequest(options);
                console.log('Update Woo product result: ' + JSON.stringify(postRs));
            } catch (err) {
                console.log(err);
            }

            await this.updateOrderDone(product.order_id, product.domain, product._id, attachmentURL, product.title);

            const sendEmail = await this.checkOrderDone(product.order_id, product.domain);
            if (sendEmail) {
                const conditions = {
                    order_id: product.order_id,
                    domain: product.domain
                };
                const order = await orderModel.findOneRecord(conditions);
                const data = {
                    customerName: product.customer_name,
                    storeName: product.store_name,
                    orderID: product.order_id,
                    attachments: order.attachments
                };
                // send email for customer
                const host = 'smtp.gmail.com';
                const port = 587;
                const username = config.email_username;
                const password = config.email_password;
                const senderName = config.email_sender_name;
                const subject = await emailService.fillParams(config.email_subject_2, data);
                const content = await emailService.fillParams(config.email_content_2, data);
                const sendMailRs = await emailService.sendMailHtml(host,
                    port,
                    username,
                    password,
                    `${senderName} <${username}>`,
                    product.email,
                    subject,
                    content);
                console.log('Send email result: ' + sendMailRs);
            }
        } catch (e) {
            console.log(e, JSON.stringify(params));
        }
    }

    async checkOrderDone(orderID, domain) {
        try {
            const conditions = {
                order_id: orderID,
                domain: domain
            };
            const order = await orderModel.findOneRecord(conditions);
            if (!order)
                console.log('checkOrderDone: order not found!');

            // this order already sent the email
            if (order.finish)
                return false;

            const products = order.products || [];
            const done = order.done || [];

            let count = 0;
            for (let i = 0; i < products.length; i++) {
                if (_.includes(done, products[i]))
                    count++;
            }

            if (count == products.length) {
                await orderModel.updateOneRecord({ _id: order._id.toString() }, { $set: { finish: true } });
                return true;
            }
        } catch (err) {
            console.log(err);
        }
    }

    async updateOrderDone(orderID, domain, productID, attachmentURL, title) {
        try {
            const conditions = {
                order_id: orderID,
                domain: domain
            };
            const order = await orderModel.findOneRecord(conditions);
            if (!order) {
                console.log('updateOrderDone: order not found!');
                return;
            }

            // set done
            let { done } = order;
            if (_.isEmpty(done))
                done = [];

            done.push(productID);

            // set attachments
            let { attachments } = order;
            if (_.isEmpty(attachments))
                attachments = [];

            const urls = _.map(attachments, att => att.url);
            if (!_.includes(urls, attachmentURL))
                attachments.push({ url: attachmentURL, title });

            await orderModel.updateOneRecord(conditions, { $set: { done, attachments } });
        } catch (err) {
            console.log(err);
        }
    }

    async getDomainFromCardName(name) {
        try {
            const index1 = name.indexOf('[');
            const index2 = name.indexOf(']');
            const domain = name.substr(index1 + 1, index2 - 1).trim();
            return domain;
        } catch (err) {
            console.log(name);
            console.log(err);
        }
    }
}

module.exports = new WebhookController();
