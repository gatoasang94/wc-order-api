/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
const appConfig = require('../../config/app');
const baseHelper = require('../helpers/BaseHelper');
const BaseController = require('./BaseController');

// models
const configModel = require('../models/Config');

class ProductController extends BaseController {
    async addOrUpdate(req, res, params) {
        try {
            const { body } = req;
            const conditions = { domain: body.domain };
            const doc = { $set: { ...body } };
            const rs = await configModel.updateOneRecord(conditions, doc, { upsert: true });

            // create trello webhook
            let url = 'https://api.trello.com/1/webhooks?';
            url += 'key=' + body.trello_key;
            url += '&token=' + body.trello_token;
            url += '&idModel=' + body.trello_dest_list;
            url += '&active=true';
            url += '&callbackURL=https://rainbowsvg.com:2096/1.0/webhook/trello';

            const options = {
                method: 'POST',
                url,
                json: {}
            };

            const result = await baseHelper.postRequest(options);
            if (result && !result.error)
                console.log('Created trello webhook for ' + body.domain);
            else if (!result || (result.error && result.error.indexOf('already exists') == -1)) {
                console.log('Another error while create webhook: ' + JSON.stringify(result));
            }

            if (rs.nModified)
                return response(res, {}, this.message.SUCCESS, statusCode.OK);
            return response(res, null, this.message.EXCEPION, statusCode.ERROR);
        } catch (e) {
            console.log(e);
            return response(res, null, this.message.EXCEPION, statusCode.ERROR);
        }
    }
}

module.exports = new ProductController();
