/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */

const appConfig = require('../../config/app');
const baseHelper = require('../helpers/BaseHelper');
const BaseController = require('./BaseController');

// models
const productModel = require('../models/Product');
const configModel = require('../models/Config');
const orderModel = require('../models/Order');

// services
const emailService = require('../../services/MailService');

class ProductController extends BaseController {
    async add(req, res, params) {
        try {
            const { body } = req;
            const imageURL = body.image_url;
            const orderID = body.order_id;
            const isFirst = body.is_first;
            const storeName = body.store_name;
            const customerName = body.customer_name;
            const {
                email, domain, sku, title
            } = body;

            console.log('====== Begin handle ', title, orderID, sku, domain);

            // find config
            let conditions = { domain };
            const config = await configModel.findOneRecord(conditions);
            if (!config)
                return response(res, null, 'Config not found!', statusCode.ERROR);

            // send email to customer if isFirst = true
            if (isFirst) {
                const data = {
                    customerName: customerName,
                    storeName: storeName,
                    orderID: orderID
                };
                const host = 'smtp.gmail.com';
                const port = 587;
                const username = config.email_username;
                const password = config.email_password;
                const senderName = config.email_sender_name;
                const subject = await emailService.fillParams(config.email_subject_1, data);
                const content = await emailService.fillParams(config.email_content_1, data);
                const sendMailRs = await emailService.sendMailHtml(host,
                    port,
                    username,
                    password,
                    `${senderName} <${username}>`,
                    email,
                    subject,
                    content);
                console.log('Send email result: ' + sendMailRs);
            }

            // create trello card
            const cardName = `[${domain}]` + encodeURI(title);
            const desc = 'SKU: ' + sku + '%0AORDER_ID: ' + orderID;
            let url = 'https://api.trello.com/1/cards?';
            url += 'key=' + config.trello_key;
            url += '&token=' + config.trello_token;
            url += '&idList=' + config.trello_source_list;
            url += '&name=' + cardName;
            url += '&pos=bottom';
            url += '&desc=' + desc;
            url += '&urlSource=' + imageURL;

            const options = {
                method: 'POST',
                url,
                json: {}
            };

            const result = await baseHelper.postRequest(options);
            if (result && result.id) {
                console.log('Product created in trello!');
            } else
                return response(res, null, 'Cannot create trello card for product!', statusCode.ERROR);

            // save DB
            const doc = {
                image_url: imageURL,
                title: title,
                order_id: orderID,
                sku: sku,
                domain,
                card_id: result.id,
                email,
                store_name: storeName,
                customer_name: customerName
            };
            conditions = {
                order_id: orderID,
                sku: sku,
                domain
            };
            let product = await productModel.updateOneRecord(conditions, doc, { upsert: true });
            if (product) {
                console.log('Product saved to DB!');
            }

            product = await productModel.findOneRecord(conditions);
            if (!product)
                console.log('Product not found after created!');

            // create order
            conditions = {
                order_id: orderID,
                domain
            };
            const order = await orderModel.findOneRecord(conditions);
            if (!order) {
                // create order
                const orderDoc = {
                    order_id: orderID,
                    domain,
                    products: [product._id.toString()]
                };
                await orderModel.createRecord(orderDoc);
            } else {
                // update order
                let productList = order.products;
                if (_.isEmpty(productList))
                    productList = [];
                productList.push(product._id.toString());
                await orderModel.updateOneRecord(conditions, { $set: { products: productList } });
            }

            return response(res, {}, this.message.SUCCESS, statusCode.OK);
        } catch (e) {
            console.log(e);
            return response(res, null, this.message.EXCEPION, statusCode.ERROR);
        }
    }
}

module.exports = new ProductController();
