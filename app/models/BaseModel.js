const mongoose = require('mongoose');

const { Schema } = mongoose;

class BaseModel {
    constructor(_name, attributes, options, _schema = false, sync_es = false) {
        let schema = _schema;
        if (!_schema) {
            schema = new Schema(attributes, options);
        }
        try {
            this.db = mongoose.model(_name, schema);
        } catch (err) {
            console.log(err);
        }
    }

    /**
     * Create document
     * @params [docs] «Array|Object» Documents to insert, as a spread or array
     * @params [options] «Object» Options passed down to save(). To specify options, docs must be an array, not a spread.  
     * Returns: «Promise»
     */
    async createRecord(docs, options = null) {
        const rs = await this.db.create(docs, options).then((rs) => {
            return rs;
        });

        return rs;
    }

    /**
     * Same as update(), except it does not support the multi or overwrite options
     * @params [conditions] «Array|Object» Documents to insert, as a spread or array
     * @params [doc] «Object» Options passed down to save(). To specify options, docs must be an array, not a spread.  
     * @params [options] «Object» optional see Query.prototype.setOptions()
     * Returns: «Query»
     */
    async updateOneRecord(conditions, doc, options = null) {
        const rs = await this.db.updateOne(conditions, doc, options).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
     * Updates one document in the database without returning it.
     * @params [conditions] «Array|Object» Documents to insert, as a spread or array
     * @params [doc] «Object» Options passed down to save(). To specify options, docs must be an array, not a spread.  
     * @params [options] «Object» optional see Query.prototype.setOptions()
     * Returns: «Query»
     */
    async updateRecords(conditions, docs, options = null) {
        const rs = await this.db.updateMany(conditions, docs, options).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
     * Finds one document.
     * @params [conditions] «Object»
     * @params [projection] «Object|String» optional fields to return, see Query.prototype.select()
     * @params [options] «Object» optional see Query.prototype.setOptions()
     * Returns: «Query»
     */
    async findOneRecord(conditions, projection = null, options = null) {
        if (conditions.$or && conditions.$or.length == 0)
            delete conditions.$or;

        const rs = await this.db.findOne(conditions, projection, options).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
     * Finds mutil documents.
     * @params [conditions] «Object»
     * @params [projection] «Object|String» optional fields to return, see Query.prototype.select()
     * @params [options] «Object» optional see Query.prototype.setOptions()
     * Returns: «Query»
     */
    async findRecords(conditions, projection = null, options = null) {
        if (conditions.$or && conditions.$or.length == 0)
            delete conditions.$or;

        const rs = await this.db.find(conditions, projection, options).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
     * Deletes the first document that matches conditions from the collection. 
     * Behaves like remove(), but deletes at most one document regardless of the single option
     * @params [conditions] «Object»
     * Returns: «Query»
     */
    async deleteOneRecord(conditions) {
        const rs = await this.db.deleteOne(conditions).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
    * Deletes all of the documents that match conditions from the collection. 
    * Behaves like remove(), but deletes all documents that match conditions regardless of the single option.
    * @params [conditions] «Object»
    * Returns: «Query»
    */
    async deleteRecords(conditions) {
        const rs = await this.db.deleteMany(conditions).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
     * Counts number of documents matching filter in a database collection.
     * @params filter «Object»
     * Returns: «Query»
     */
    async countRecords(conditions) {
        let rs = null;
        if (conditions.$or && conditions.$or.length == 0)
            delete conditions.$or;

        if (Object.keys(conditions).length == 0) {
            rs = await this.db.estimatedDocumentCount(conditions).then((rs) => {
                return rs;
            });
        } else {
            rs = await this.db.countDocuments(conditions).then((rs) => {
                return rs;
            });
        }
        return rs;
    }

    /**
     * Creates a Query for a distinct operation.
     * @params field «String»
     * @params [conditions] «Object» optional
     * Returns: «Query»
     */
    async distinctRecords(field, conditions = null) {
        if (conditions.$or && conditions.$or.length == 0)
            delete conditions.$or;

        const rs = await this.db.distinct(field, conditions).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
     * 
     * @param {*} conditions 
     */
    async hardDeleteOneRecord(conditions) {
        const rs = await this.db.findOneAndDelete(conditions).then((rs) => {
            return rs;
        });
        return rs;
    }

    /**
    * 
    * @param {*} docs 
    */
    async insertMany(docs) {
        const rs = await this.db.insertMany(docs).then((rs) => {
            return rs;
        });
        return rs;
    }
}

module.exports = BaseModel;