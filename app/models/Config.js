const BaseModel = require('./BaseModel');

class Config extends BaseModel {
    constructor() {
        const _name = 'config';
        const attributes = {

            domain: { type: String, index: true },

            trello_key: String,

            trello_token: String,

            trello_board: String,

            trello_source_list: String,

            trello_dest_list: String,

            email_username: String,

            email_password: String,

            email_sender_name: String,

            email_subject_1: String,

            email_content_1: String,

            email_subject_2: String,

            email_content_2: String
        };
        const options = {
            collection: 'configs',
            timestamps: {
                createdAt: 'created_at',
                updatedAt: 'updated_at'
            },
            versionKey: false
        };

        super(_name, attributes, options);
    }
}

module.exports = new Config();
