const BaseModel = require('./BaseModel');

class Product extends BaseModel {
    constructor() {
        const _name = 'product';
        const attributes = {
            image_url: String,

            title: String,

            order_id: { type: String, index: true },

            sku: { type: String, index: true },

            domain: String,

            card_id: { type: String, index: true },

            is_done: { type: Boolean, default: false },

            email: String,

            customer_name: String,

            store_name: String,
        };
        const options = {
            collection: 'products',
            timestamps: {
                createdAt: 'created_at',
                updatedAt: 'updated_at'
            },
            versionKey: false
        };

        super(_name, attributes, options);
    }
}

module.exports = new Product();
