const BaseModel = require('./BaseModel');

class Order extends BaseModel {
    constructor() {
        const _name = 'order';
        const attributes = {

            order_id: { type: String, index: true },

            domain: String,

            products: [String],

            done: [String],

            attachments: [Object],

            finish: Boolean
        };
        const options = {
            collection: 'orders',
            timestamps: {
                createdAt: 'created_at',
                updatedAt: 'updated_at'
            },
            versionKey: false
        };

        super(_name, attributes, options);
    }
}

module.exports = new Order();
