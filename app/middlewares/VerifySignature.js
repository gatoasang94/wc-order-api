const appConfig = require('../../config/app');
const baseHelper = require('../helpers/BaseHelper');

class VerifySignature {
    constructor(req, res) {
        this.req = req;
        this.res = res;
    }

    /**
     * 
     */
    async handle() {
        try {
            return await this.validate(this.req);
        } catch (err) {
            return { isValid: false };
        }
    }

    async validate(req) {
        const originBody = _.cloneDeep(req.body);

        const signature = req.headers['X-Hub-Signature'] || req.headers['x-hub-signature'];
        if (!signature) {
            return { isValid: false };
        }

        const secret = appConfig.AUTH.SECRET_KEY;

        if (signature.substr(0, 5) == 'sha1=') {
            const { timestamp } = req.headers;
            const hashStr = baseHelper.getSignatureBySecretKey(baseHelper.getSortedStringFromObject(originBody), secret || '');
            const tsHash = baseHelper.getSignatureBySecretKey(timestamp.toString(), secret || '').substr(5, 40);

            return { isValid: hashStr + '.' + tsHash === signature, role: originBody.role };
        }

        return { isValid: false };
    }
}

module.exports = VerifySignature;
