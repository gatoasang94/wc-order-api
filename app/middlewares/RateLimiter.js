const rateLimit = require('express-rate-limit');

const limiter = rateLimit({
    windowMs: 1000, // 100 miliseconds
    max: 10, // limit each IP to 1 requests per windowMs,
    delayMs: 0,
    handler: handlerMaxRequest,
});

function handlerMaxRequest(req, res) {
    const result = {
        status: false,
        message: 'Too Many Requests!'

    };
    res.status(426).end(JSON.stringify(result));
}

module.exports = limiter;