const baseHelper = require('../helpers/BaseHelper');

const userScopes = baseHelper.fileToJSON(`${__dirname}/../../config/user_scopes.json`);

class VerifyUserScope {
    constructor(req, res, role) {
        this.req = req;
        this.res = res;
        this.role = role;
    }

    async handle() {
        return this.checkUserScope(this.role);
    }

    checkUserScope(role) {
        const avaiableScopes = userScopes[role];

        if (avaiableScopes == true)
            return true;

        const path = this.req.originalUrl.split('/');

        let [, , controller, action] = path;

        action = action.replace(/-/g, '_');

        controller = controller.replace(/-/g, '_');

        if (controller == 'tools' && action == 'trim_excel')
            return true;

        if (controller == 'account' && action == 'import')
            return true;

        const avaiableActions = avaiableScopes[controller];

        if (avaiableActions) {
            if (avaiableActions == true) {
                return true;
            }

            if (_.includes(avaiableActions, action)) {
                return true;
            }
        }

        return false;
    }
}

module.exports = VerifyUserScope;
