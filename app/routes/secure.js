const express = require('express');
const path = require('path');
const fs = require('fs');

const multer = require('multer');
const limiter = require('../middlewares/RateLimiter');

const VerifySignature = require('../middlewares/VerifySignature');
const VerifyUserScope = require('../middlewares/VerifyUserScope');

const baseHelper = require('../helpers/BaseHelper');
const cf = require('../../config/config');

const upload = multer({ dest: 'uploads/' });

const message = require('../../languages/language')();

const router = express.Router();

const uploadRouters = [
    {
        path: '/1.0/tools/trim-excel',
        controller: 'tools',
        functionName: 'trim-excel',
    },
    {
        path: '/1.0/account/import',
        controller: 'account',
        functionName: 'import',
    },
];

router.use(baseHelper.trimBody, async (req, res, next) => {
    next();
});

router.use(async (req, res, next) => {
    const verifySignature = new VerifySignature(req, res);
    const result = await verifySignature.handle();
    if (!result.isValid) {
        return response(res, {}, message.UNAUTHORIZED, statusCode.UNAUTHORIZED);
    }

    const verifyUserScope = new VerifyUserScope(req, res, result.role);
    const isValid = await verifyUserScope.handle();
    if (!isValid)
        return response(res, {}, message.SCOPE_INVALID, statusCode.FORBIDDEN);

    next();
});

(async () => {
    const dir = `${__dirname}/../controllers/`;
    fs.readdirSync(dir).filter((file) => {
        return (file.indexOf('.') !== 0) && (file !== 'BaseController.js');
    }).forEach((file) => {
        try {
            const _path = dir + path.sep + file;

            const controllerName = setControllerName(file);
            let controller;
            try {
                controller = require(_path);
            } catch (err) {
                console.log(err);
            }

            const actions = Object.getOwnPropertyNames(Object.getPrototypeOf(controller));

            for (let i = 0; i < actions.length; i++) {
                const action = actions[i];
                const actionPath = setActionPath(action);

                if (action != 'constructor') {
                    const isUploadRouter = _.find(uploadRouters, item =>
                        item.controller == controllerName && item.functionName == actionPath);

                    if (isUploadRouter) {
                        router.post(`/${cf.API_VERSION}/${controllerName}/${actionPath}`, upload.any(), limiter, async (req, res) => {
                            const startRequest = new Date().getTime();
                            await controller.setLanguage(req.body.lang);
                            await controller[action](req, res, _.cloneDeep(req.body));

                            const endRequest = new Date().getTime();

                            logRequest(req, res, endRequest - startRequest);
                            return;
                        });
                    } else {
                        router.post(`/${cf.API_VERSION}/${controllerName}/${actionPath}`, limiter, async (req, res) => {
                            const startRequest = new Date().getTime();
                            await controller.setLanguage(req.body.lang);
                            await controller[action](req, res, _.cloneDeep(req.body));

                            const endRequest = new Date().getTime();

                            logRequest(req, res, endRequest - startRequest);
                            return;
                        });
                    }
                }
            }
        } catch (err) {
            console.log(err);
        }
    });
})();

function setActionPath(action) {
    const _action = action.replace(/(?:^|\.?)([A-Z])/g, (x, y) => {
        return `-${y.toLowerCase()}`;
    }).replace(/^_/, '');
    return _action;
}

function setControllerName(controllerName) {
    let _controllerName = controllerName.replace(/(?:^|\.?)((Controller.js))/g, (x, y) => {
        return '';
    }).replace(/^_/, '');
    _controllerName = _controllerName.replace(/(?:^|\.?)([A-Z])/g, (x, y) => {
        return `-${y.toLowerCase()}`;
    }).replace(/^_/, '');
    return _controllerName.slice(1, _controllerName.length);
}

module.exports = router;
