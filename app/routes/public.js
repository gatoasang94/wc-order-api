const express = require('express');
const limiter = require('../middlewares/RateLimiter');

const cf = require('../../config/config');
const appConfig = require('../../config/app');
const WebhookController = require('../controllers/WebhookController');
const ProductController = require('../controllers/ProductController');
const ConfigController = require('../controllers/ConfigController');

const message = require('../../languages/language')();

const router = express.Router();

const routerConfig = [
    {
        path: '/' + cf.API_VERSION + '/webhook/trello',
        controller: WebhookController,
        functionName: 'monitor',
        allowIp: [],
        method: 'POST',
        isAuth: false,
    },
    {
        path: '/' + cf.API_VERSION + '/product/add',
        controller: ProductController,
        functionName: 'add',
        allowIp: [],
        method: 'POST',
        isAuth: true,
    },
    {
        path: '/' + cf.API_VERSION + '/config/create-or-update',
        controller: ConfigController,
        functionName: 'addOrUpdate',
        allowIp: [],
        method: 'POST',
        isAuth: true,
    }
];

router.use(async (req, res, next) => {
    try {
        const pathRequest = req.path;
        const index = _.findIndex(routerConfig, (o) => {
            return o.path == pathRequest;
        });

        const ipAdress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

        if (index >= 0) {
            const checkValidIp = (routerConfig[index].allowIp.length == 0
                || _.includes(routerConfig[index].allowIp, ipAdress));
            if (!checkValidIp) {
                return response(res, {}, message.IP_NOT_ALLOWED, statusCode.ERROR);
            }

            if (routerConfig[index].isAuth) {
                const auth = req.headers.authorization;
                if (!auth || auth != appConfig.LOCAL_SECRET_KEY) {
                    return response(res, {}, message.UNAUTHORIZED, statusCode.ERROR);
                }
            }
        }
        next();
    } catch (err) {
        logStack(err);
        return response(res, {}, message.PROCESS_ERROR, statusCode.ERROR);
    }
});

routerConfig.forEach((objRouter) => {
    if (objRouter.method == 'GET') {
        router.get(objRouter.path, limiter, async (req, res) => {
            try {
                objRouter.controller.setLanguage(req.body.lang);
                const startRequest = new Date().getTime();
                await objRouter.controller[objRouter.functionName](req, res, req.query);
                const endRequest = new Date().getTime();
                logRequest(req, res, endRequest - startRequest);
                return;
            } catch (err) {
                logStack(err);
            }
        });
    } else {
        router.post(objRouter.path, limiter, async (req, res) => {
            try {
                objRouter.controller.setLanguage(req.body.lang);
                const startRequest = new Date().getTime();
                await objRouter.controller[objRouter.functionName](req, res, req.body);
                const endRequest = new Date().getTime();
                logRequest(req, res, endRequest - startRequest);
                return;
            } catch (err) {
                logStack(err);
            }
        });
    }
});

module.exports = {
    router,
    routerConfig: routerConfig,
};
