module.exports = {
    apps: [{
        name: 'wc-order-api',
        script: 'app.js',
        instances: '1',
        autorestart: true,
        increment_var: 'PORT',
        env: { NODE_ENV: 'develop' },
        env_production: { NODE_ENV: 'production' }
    }],
};
