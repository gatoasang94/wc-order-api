/* eslint-disable max-len */
const nodemailer = require('nodemailer');
const baseHelper = require('../app/helpers/BaseHelper');
const he = require('he');

class MailService {
    async testConnectSMTP(options) {
        try {
            const transport = nodemailer.createTransport(options);
            return new Promise(((resolve, reject) => {
                try {
                    transport.verify((error) => {
                        if (error) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    });
                } catch (err) {
                    reject(err);
                }
            }));
        } catch (err) {
            logStack(err);
        }
    }

    async sendMailHtml(host, port, username, password, from, to, subject, html) {
        const transport = nodemailer.createTransport({
            host: host,
            port: port,
            secure: port == 465,
            auth: {
                user: username,
                pass: password,
            }
        });

        const message = {
            from: from, // Sender address
            to: to, // List of recipients: a@gmail.com, b@gmail.com, c@gmail.com
            subject: subject, // Subject line
            html: html // html
        };
        const sendMail = await transport.sendMail(message).then(() => true)
            .catch((e) => { console.log(e); return false; });
        return sendMail;
    }

    async fillParams(text, data) {
        try {
            text = text.split('{{name}}').join(data.customerName);
            text = text.split('{{storeName}}').join(data.storeName);
            text = text.split('{{orderID}}').join(data.orderID);

            if (!_.isEmpty(data.attachments)) {
                const downloadHTML = await this.getDownloadHTML(data.attachments);
                text = text.split('{{download}}').join(downloadHTML);
            }

            return he.decode(text);
        } catch (err) {
            console.log(err);
            return text;
        }
    }

    async getDownloadHTML(attachments) {
        try {
            const tableHTML = `<table border="1" style="border-collapse: collapse; width: 100%; height: auto;">
            <tbody>
            <tr style="height: 22px;">
            <td style="width: 42.8012%; text-align: left; height: 22px;"><strong>Product</strong></td>
            <td style="width: 11.9887%; text-align: left; height: 22px;"><strong>Expires</strong></td>
            <td style="width: 40.431%; text-align: left; height: 22px;"><strong>Download</strong></td>
            </tr>
            ${await this.getProductRow(attachments)}
            </tbody>
            </table>`;

            return tableHTML;
        } catch (err) {
            console.log(err);
        }
    }

    async getProductRow(attachments) {
        try {
            let rs = '';
            for (let i = 0; i < attachments.length; i++) {
                const attachment = attachments[i];
                rs += `<tr style="height: 22px;"><td style="width: 42.8012%; text-align: left; height: 22px;">${attachment.title}</td>
                <td style="width: 11.9887%; text-align: left; height: 22px;">Never</td>
                <td style="width: 40.431%; text-align: left; height: 22px;"><a href="${attachment.url}">Download</a></td></tr>`;
            }

            return rs;
        } catch (err) {
            console.log(err);
        }
    }
}

module.exports = new MailService();
