const schedule = require('node-schedule');
const Crawler = require('crawler');
const rp = require('request-promise');
const cheerio = require('cheerio');
const appConfig = require('../config/app');

const baseHelper = require('../app/helpers/BaseHelper');

class SchedulerService {
    async checkStartService() {
        try {
        } catch (err) {
            logStack(err);
        }
    }

    async startSchedule() {
        try {
            await this.checkStartService();

            // schedule every day
            schedule.scheduleJob('0 0 1 * * *', () => {
            });
        } catch (err) {
            console.log(err);
        }
    }
}

module.exports = SchedulerService;
