module.exports = {

    DEFAULT_LIMIT: 10,

    DEFAULT_PAGE: 1,

    STATUS: {
        INACTIVE: 0,
        ACTIVE: 1,
        NOT_DELETE: false,
        DELETED: true,
    },

    COMMON_STATUS: {
        ACTIVE: true,
        DEACTIVE: false,
        ASCENDING: 1,
        DESCENDING: -1
    },

    AUTH: { SECRET_KEY: '8y/B?E(H+MbQeThWmZq4t7w9z$C&F)J@' },

    LOCAL_SECRET_KEY: '969208b4-5488-11eb-ae93-0242ac130002',
};
