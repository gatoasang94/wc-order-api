const compression = require('compression');
const express = require('express');
const config = require('config');
const helmet = require('helmet');
const app = express();
const device = require('express-device');
const bodyParser = require('body-parser');
var https = require('https');
var http = require('http');
var fs = require('fs');

app.use(device.capture());
app.use(compression());
// App Configuration with Swagger
app.set('port', process.env.PORT || config.get('server.port'));

app.use(helmet.frameguard()); // defaults to sameorigin
app.use(helmet.xssFilter());
app.use(helmet.noSniff());
app.use(helmet.ieNoOpen());
// Hide X-Powered-By
app.use(helmet.hidePoweredBy());

app.disable('x-powered-by');
app.enable('trust proxy');
app.set('trust proxy', true);

app.all('/*', (req, res, next) => {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Hub-Signature, timestamp, Access-Control-Allow-Credentials, Store-id');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Max-Age', 2592000);
    if (req.method === 'OPTIONS') {
        res.writeHead(200);
        res.end();
    } else {
        next();
    }
});

// body parse
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
// Config global
require('./global')();

// Use Routes & Boot system
app.use(require('./app/routes/public').router);
app.use(require('./app/routes/secure'));

// Connect database
require('./app/connections/mongoDB');

// Start server
const server = app.listen(config.get('server.port'), config.get('server.host'), () => {
    console.log('=================Server WC-Order-API start at port:' + server.address().port + ' Time: ' + new Date());
    monitor.notifyErrorFunction(process.env.NODE_ENV + '=================Server pin start at port:' + server.address().port + ' Time: ' + new Date());
    const SchedulerService = require('./services/SchedulerService');
    const schedule = new SchedulerService();
    schedule.startSchedule();
});

// Certificate
const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${config.get('domain')}/privkey.pem`, 'utf8');
const certificate = fs.readFileSync(`/etc/letsencrypt/live/${config.get('domain')}/cert.pem`, 'utf8');
const ca = fs.readFileSync(`/etc/letsencrypt/live/${config.get('domain')}/chain.pem`, 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};

// api HTTPS
const httpsServer = https.createServer(credentials, app);
httpsServer.listen(2096, () => {
});

module.exports = app;
